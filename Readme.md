# A64Dbg on Windows

#### Description

An arm64 assembly level debugger for Android on Windows.

Note: The backend lldb/python runtime only support at least Windows 10.

Follow us for update or bug report:

|Platform|Account|
|-|-|
|Email|liubaijiang@yunyoo.cn|
|公众号|刘柏江|
|头条抖音|刘柏江|
|微博|刘柏江VM|
|码云|https://gitee.com/geekneo/|
